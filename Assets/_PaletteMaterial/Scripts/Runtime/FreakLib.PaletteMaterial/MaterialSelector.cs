﻿
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;


namespace FreakLib.PaletteMaterial
{
    [ExecuteInEditMode]
  [RequireComponent(typeof(MeshFilter))]
  public class MaterialSelector : MonoBehaviour
  {
    [SerializeField] private int _atlasSize = 4;
    [SerializeField] private int[] _atlasIndex = new int[] { 0 };

        void OnEnable()
    {
      UpdatePaletteIndex();
    }

    public void UpdatePaletteIndex()
    {
            if (_atlasIndex == null || _atlasIndex.Length <= 0)
            {
                Debug.LogError("Atlas index array not set");
                return;
            }

      MeshFilter meshFilter = GetComponent<MeshFilter>();
            Mesh mesh;
#if UNITY_EDITOR
            Mesh meshCopy = Mesh.Instantiate(meshFilter.sharedMesh);
            meshCopy.name = meshFilter.sharedMesh.name;
            mesh = meshFilter.mesh = meshCopy;
#else
            Mesh mesh =  meshFilter.mesh;
#endif // UNITY_EDITOR
            int subMeshCount = mesh.subMeshCount;
            Vector2[] uvs = mesh.uv;

            for (int i = 0; i < subMeshCount; ++i)
            {
                int index =
                    (i < _atlasIndex.Length) ?
                    _atlasIndex[i] :
                    _atlasIndex[_atlasIndex.Length - 1];

                int indexX = index % _atlasSize;
                int indexY = index / _atlasSize;

                float tileSize = 1f / (float)_atlasSize;
                float offset = tileSize / 2f;

                Vector2 uv =
                    new Vector2(
                        ((float)indexX * tileSize) + offset,
                        ((float)indexY * tileSize) + offset);

                int[] triangles = mesh.GetTriangles(i);
                for (int j = 0; j < triangles.Length; ++j)
                {
                    int triangleIdx = triangles[j];
                    uvs[triangleIdx] = uv;
                }
            }

            mesh.uv = uvs;
    }
  }
}
