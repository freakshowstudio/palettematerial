﻿
#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

using FreakLib.PaletteMaterial;


namespace FreakLibEditor.PaletteMaterial
{
    [CustomEditor(typeof(MaterialSelector))]
    public class MaterialSelectorInspector : Editor
    {
        private static readonly string[]    _atlasSizeNames = new string[]
        {
            "2x2", "4x4", "8x8", "16x16", "32x32", "64x64", "128x128", "256x256"
        };

        private static readonly int[]       _atlasSizeValues = new int[]
        {
            2, 4, 8, 16, 32, 64, 128, 256
        };

        public override void OnInspectorGUI()
        {
            MaterialSelector selector = target as MaterialSelector;


            SerializedProperty atlasSizeProperty =
                serializedObject.FindProperty("_atlasSize");

            SerializedProperty atlasIndexArrayProperty =
                serializedObject.FindProperty("_atlasIndex");


            atlasSizeProperty.intValue =
                EditorGUILayout.IntPopup(
                    "Atlas Size",
                    atlasSizeProperty.intValue,
                    _atlasSizeNames,
                    _atlasSizeValues);

            MeshFilter selectorMeshFilter = selector.GetComponent<MeshFilter>();
            Mesh selectorMesh = selectorMeshFilter.sharedMesh;
            int selectorSubMeshCount = selectorMesh.subMeshCount;

            atlasIndexArrayProperty.arraySize = selectorSubMeshCount;

            for (int i = 0; i < selectorSubMeshCount; ++i)
            {
                SerializedProperty indexElementProperty =
                    atlasIndexArrayProperty.GetArrayElementAtIndex(i);

                string label = String.Format("Submesh {0} Palette Index", i);
                indexElementProperty.intValue =
                    EditorGUILayout.IntField(
                        label, indexElementProperty.intValue);

                int maxSize =
                    (atlasSizeProperty.intValue *
                     atlasSizeProperty.intValue) - 1;

                indexElementProperty.intValue =
                    Mathf.Clamp(indexElementProperty.intValue, 0, maxSize);
            }

      bool didUpdate = serializedObject.ApplyModifiedProperties();

      if (GUILayout.Button("Update") || didUpdate)
      {
        selector.UpdatePaletteIndex();
      }
        }
  }
}
#endif // UNITY_EDITOR
